from scapy.all import *
import re


WEATHER_IP = '34.218.16.79'
WEATHER_WELCOME_MSG = 'Welcome to Magshimim\'s Forecast Server!'

HTTP_PORT = 80
GET_COMMAND = 'GET'
START_INDEX = len(GET_COMMAND) + 1
END_INDEX = "HTTP"

DNS_RESPONSE_TYPE = 1

PATTERN = r'[\w.-]+%40[\w]+.\w+'
MAIL_SYMBOL = '@'
MAIL_SYMBOL_SCAPY = '%40'

FILTER_INDEX = 0
PRN_INDEX = 1

def dns_filter(packet):
    """
    function get packet, and check if he contains the layer packet[DNS].an[DNSRR], and the type equal to 1 (packet contains IP)

    :param packet: a packet to check
    :type packet: not same, can get many types of protocols
    :return: True -> the packet is a DNS response, else False
    :rtype: bool
    """
    return DNS in packet and \
           packet[DNS].an != None and \
           DNSRR in packet[DNS].an and \
           packet[DNS].an[DNSRR].type == DNS_RESPONSE_TYPE


# function return string "[*] url: ip"
dns_info = lambda packet: f"[*] {packet[DNS].an[DNSRR].rrname.decode()}: {packet[DNS].an[DNSRR].rdata}"


def weather_filter(packet):
    """
    function get packect, and check if it a packet from the weather server

    :param packet: a packet to check
    :type packet: not same, can get many types of protocols
    :return: True -> the packet is a weather-server answer, else False
    :rtype: bool
    """
    return IP in packet and \
           packet[IP].src == WEATHER_IP and \
           Raw in packet and \
           not(WEATHER_WELCOME_MSG in packet[Raw].load.decode())

# function return string "[*] weather-server answer"
weather_info = lambda packet: f"[*] {packet[Raw].load.decode()}"


def get_filter(packet):
    """
    function get packet and check if it HTTP get request

    :param packet: a packet to check
    :type packet: not same, can get many types of protocols
    :return: True -> the packet is a HTTP GET request, else False
    :rtype: bool
    """
    return (Raw in packet and GET_COMMAND in str(packet[Raw])) and \
           (TCP in packet and packet[TCP].dport == HTTP_PORT)


def get_info(packet):
    """
    function get http request packet, and return a string of the request

    :param packet: a packet to check
    :type packet: not same, can get many types of protocols
    :return: string "[*] {request}"
    :rtype: str
    """
    request = str(packet[Raw])
    return f"[*] {request[request.index(GET_COMMAND) + START_INDEX: request.index(END_INDEX)]}"


# function check if it HTTP packet, that contains data (Raw)
email_filter = lambda pack: (Raw in pack and TCP in pack) and (pack[TCP].sport == HTTP_PORT or pack[TCP].dport == HTTP_PORT)


def email_info(packet):
    """
    function get packet, and search all the email in the packet, and print them

    :param packet: HTTP packet
    :type packet: scapy.layers.HTTP
    :return: none
    """
    if len(packet[Raw].load):
        emails = re.findall(PATTERN, str(packet[Raw].load))
        for pack in emails:
            print(f"[*] {pack.replace(MAIL_SYMBOL_SCAPY, MAIL_SYMBOL)}")

options = {
    '1': (dns_filter, dns_info),
    '2': (weather_filter, weather_info),
    '3': (get_filter, get_info),
    '4': (email_filter, email_info)
}


def main():
    while True:
        try:
            user_choice = input("\n\n1 - dns\n2 - weather\n3 - HTTP GET\n4 - Email in GET\n>>")
            sniff(lfilter=options[user_choice][FILTER_INDEX], prn=options[user_choice][PRN_INDEX])

        except (KeyboardInterrupt, EOFError, KeyError):
            continue




if __name__ == '__main__':
    main()
